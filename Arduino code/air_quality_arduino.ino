#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define pixelPin    21 // Which pin on the Arduino is connected to the NeoPixels?
#define LED_COUNT 7 // How many NeoPixels are attached to the Arduino?

#define dustPin A0 // Black wire analog input (say A0)
#define ledPin 5  // White wire digital output (to control the LED)
#define ANALOG_VOLTAGE 3.3 // analog top of range
// Yellow, Green wire connects to ground
// Blue wire connects to 5V via 150 ohm resistor (see DS)
// Red wire connects to 5V

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, pixelPin, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed

void setup()
{
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
//  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)
  
//  Serial.begin(115200);
//while (!Serial) delay(100);
//  Serial.println("GP2Y1010AU0F Sensor demo");
pinMode(ledPin, OUTPUT);
}

void loop()
{
float output_voltage, dust_density;
digitalWrite(ledPin, LOW); // power on the LED
delayMicroseconds(280); // Wait 0.28ms according to DS
// take analog reading
  output_voltage = analogRead(dustPin); 
delay(1);
digitalWrite(ledPin, HIGH); // turn the LED off
  output_voltage = (output_voltage / 4095) * ANALOG_VOLTAGE;//1023
  dust_density = 1000*((0.18 * output_voltage) );//- 0.1
//  Serial.print("Voltage = ");
//  Serial.print(output_voltage);
//  Serial.print(",\tDust Density = ");
//  Serial.print(dust_density);
//  Serial.println(" ug/m3");
  if (dust_density<50) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, strip.Color(8, 255,   32));         //  Set pixel's green (in RAM)
    strip.show();                          //  Update strip to match
  }
  }
  else if (dust_density>100) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, strip.Color(255,   8,   32));         //  Set pixel's red (in RAM)
    strip.show();                          //  Update strip to match
  }
  }
  else {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, strip.Color(255,   128,   0));         //  Set pixel's orange (in RAM)
    strip.show();                          //  Update strip to match
  }
  }
delay(2000); //purple strip.Color(78,   0,   142)
}
