# Master in Design for Emerging Futures
# Micro Challenge 2 MDEF / Pascal, The Chameleon: An Air Quality Monitoring Station 
- Rita Veronica Agreda de Pazos, Francisco Flores & Clement Rames
- MDEF 2020/21
- Institut d'Arquitectura Avançada de Catalunya, Elisava & Fab Lab Barcelona
## Table of Contents
1. [Creative education and cocreation](#creative-education-and-cocreation)
2. [Context and Problems](#context-and-problems)
3. [Current situation](#current-situation)
4. [Pascal and the Leaf](#pascal-and-the-leaf)
5. [Iteration 1](#iteration-1)
6. [Iteration 2](#iteration-2)
7. [Final Presentation](#final-presentation)
8. [Concepts](#concepts)
9. [Skills](#skills)
## Creative education and cocreation
Imagine a world where all people have equal rights and opportunities and where we can breathe clean air in every city. In this world #GenderEquality and #FairMobility are the norms...  Creating synergies between our fights, we started by designing a learning experience that involve us, as designers, and children in creative education and a cocreation project.

The aim of our project was to collaborate with young children for educational, co-design and co-creation purposes while making an Air Quality Monitoring Station and developing children’s STEAM (Science, Technology, Engineering, Arts and Mathematics) skills. 
## Context and problems
**Clean air for all: measuring air quality can save lives! **

Air Pollution is an urgent danger to public health. It is time to take action!

Outdoor Air Pollution is responsible for 4.2 million deaths. More than 90% of the world’s population lives in areas that exceed World Health Organization guidelines for ambient air quality.

Children born in areas of high air pollution are likely to have several years taken off their life expectancy.

The **air pollution causes** are often the same as climate change: transport, **power sector and industrial emissions.**
## Current situation 
Technology allows us to measure our exposure to air pollution much better than ever before and design invite us to find new ways to deploy in mobility, energy and manufacturing.
## Pascal and the Leaf
By tackling air quality issues, we can save lives and we can also inject urgency into the **climate change agenda. **

_What happens when we involve children’s creativity, IoT and ancestor’s tales to co-create an educative and communicational artifact?_

Alexandra Pazos (8) and Helena Pazos (7) were invited to our ideation working sessions of our Air quality monitoring. They were aware of the impacts of polluted air in human’s health and planetary ecosystems, so they purposed to use a **chameleon to check the Air Quality**. They wanted to use his/her superpowers and abilities of changing colors to let him/her tell us if the air was clean and healthful or not. They baptized the chameleon **Pascal**. 

As designers we guided them trough the designing process, from drawing sketches, plans, views, and perspectives, building the playdough’s models, 3D scanning ([Qlone](https://www.qlone.pro)), sculpting and 3d modeling ([Autodesk Meshmixer](https://www.meshmixer.com)) to 3d printing ([Cura](https://ultimaker.com/es/software/ultimaker-cura)).  
### Drawing a Chameleon from different points of views and plans
 Alexandra and Helena draw Pascal from different points of view and plans.
![](Images/Chameleon.jpg)
![](Images/Chameleon_1.jpg)
### 3d Scanning and 3D Modeling Pascal
**Step 1**:
Alexandra and Helena made a Pascal's model in Play Doug(analog version). 

**Step 2**:
We installed [Qlone](https://www.qlone.pro) in Alexandra's tablet. 

**Step 3**:
We printed a [mesh](https://28201f68-fc5e-48bf-ae38-d8fec5beca48.filesusr.com/ugd/0dc13a_00f1c793e9274ea4897766276c116ca1.pdf) adapted to our model.

**Step 4**:
We [3D scan](Images/3d_scannig_Qlone.mp4) the Play Doug's model of Pascal.

To watch the video of the process :) ![click here](Images/3d_scannig_Qlone.mp4) 

![](Images/3d_scan.png)
![](Images/3D_scan_1.png)

**Step 5:**
We [3D modeled and sculpt](Images/3D_modeling_meshmixer.mp4) the digital version of Pascal with [Meshmixer](https://www.meshmixer.com) and we fixed Pascal's tail.

Watch the video of the 3D modeling process :) ![Click here](Images/3D_modeling_meshmixer.mp4) 

![](Images/3d_modeling.png)

Enjoying Pascal's capacities to change color and using his/her leaf, were able to measure the air quality and visualize the information of the air quality in a friendly and inclusive way using a color code (red: critical, orange: mediocre and green: normal).

Pascal's Leaf became the Air Quality Monitoring Station. It was a perfect place to place the electronics in a safe and fonctional way. To design and vectorize the Leaf sketches we used [Adobe Illustrator](https://www.adobe.com/es/products/illustrator/free-trial-download.html). 

![](Assets/Leaf_base_sketch_1.png)
![](Assets/Leaf_cover_sketch.png)
# Iteration 1
### Materials
- 3D printer
- Translucent PLA filament
- Lasercutter or a cutter
- Cardboard
- Dust sensor
- micro:bit board
- Iot:bit
- OLED display
- Adafruit Neopixel LEDD strip
- A smartphone, or a tablet, or a computer.
- Internet connection
### 3D Printing with translucid PLA 
To let Pascal change colors he/she has to be transparent or translucent. Our first Pascal's prototype was 3D printed in translucid PLA. 

![](Images/Pascal_rainbow.jpg)

The first prototype of his/her Leaf was in cardboard. We lasercutted it. This cardboard prototype let us figure out the best way to integrate the electronics. 
### Electronics
In our first iteration, we discovered the potential of a block-based educational coding plataform: [Makecode](https://makecode.microbit.org/#). We used a micro:bit board, a [Smart Science IoT Kit](https://www.elecfreaks.com/learn-en/microbitKit/iot_kit/iot_kit.html) and the [Air quality monitoring project](https://www.elecfreaks.com/learn-en/microbitKit/iot_kit/IOT_bit_case01.html) as reference to start the coding.

**Smart Science IoT Kit**

![](Images/Smart_Science_IoT_Kit.jpg) ![](Images/iot_bit_03.png)
### Diagram
![](Images/case_01_021.png)
### Code
**The Air quality monitoring project** in MakeCode plataform

![](Images/Makecode.png)

Watch the video of our Air quality monitoring station with micro:bit, a dust sensor, an IoT:bit, an OLED display and an Adafruit Neopixel LED strip.

![click here](Images/Coding_microbit.mp4)
## Iteration 2
After the first prototype, Alexandra and Helena wanted to sleep with Pascal and to bring he/she to the park to mesure the air quality and to hunt the smoking people. :)

For this iteration we wanted Pascal to glow in the dark and to have a wood leaf.

The final version is made of plywood (3 layers cutted with the CNC and glued) and a 6 mm acrylic (cutted with the laser machine) to make visible from the bottom how the electronics and the sensor are connected.
### Materials
- 3D printer
- [PLA flex filament](https://www.smartmaterials3d.com/en/flex-filament), glows in the dark
- CNC
- 12mm plywood 
- Lasercutter 
- 3mm acrylic
- Dust sensor[(Sharp dust sensor GP2Y1010AU0F](https://global.sharp/products/device/lineup/data/pdf/datasheet/gp2y1010au_appl_e.pdf))
- [Adafruit HUZZAH32 – ESP32 Feather](https://www.adafruit.com/product/3591) board
- [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel) LEDD strip
- A 3.7V, 2000 mAh Lithium-Polymer rechargeable battery
- A smartphone, or a tablet, or a computer.
- Internet connection
- [Arduino](https://www.arduino.cc/en/software) IDE Software
- [Neopixel library](https://www.arduinolibraries.info/libraries/adafruit-neo-pixel) & sensor interface
### 3D Printing with translucid PLA flex filament
To let Pascal change colors, be soft, squeeze and glow in the nigth be 3D printed he/she translucid PLA flex filament. 

**Step 0:** Be sure you choose the hollow option before to exporting Pascal STL file from Meshmixer.  

**Step 1:** Open the STL file in Cura 

**Step 2:** Verify the model touchs the plane

**Step 3:** To see the first printing point use the Slice preview command

**Step 4:** Setting Cura printing parameters 
-  Skirt/Brim Line With: 0.4
- Support Line With: 0.4
- Initial Layer Line With: 100.0
- Shell 
-  Infill: 0
-  Material 
- [x] Printing temperature: 230,
- [x] Printing temperature initial layer: 230
- [x] Initial printing temperature: 230
- [x] Final printing temperature: 230
- [x] Build plate adhesion: 0
- [x] Build plate temperature initial layer: 0
-  Supports: columns

![](Images/Cura_slicer.jpeg)
![](Images/Setting-cura_flexglow.jpeg)
![](Images/Pascal_flexglow.jpeg)

Watch the video of the process :) ![click here](Images/3D_printing_with_Cura.mp4)  
### CNC and Lasercutter
The final version is made of plywood.

It has **3 layers**. The layer were cut and milled with the CNC and glued togheter. 

**Setting the CNC parameters**
![](Images/CNC_settings.jpeg)

**Cutting and milling with the CNC**
![](Images/cnc.jpeg)

* Safety recomendations: **Protect yourself!**
![](Images/CNC_cutting.jpeg)

To make visible how the electronics and the sensor are connected from the bottom part of the leaf, we choose to make it in a 6 mm acrylic that we cutted with the lasercutter.

![](Images/Bottom.jpg)

Watch the video of the process of the leaf fabrication with the CNC and the lasercutter.
![click here](Images/Pascals_leaf_CNC.mp4)
### Electronics design
The electronics for this project are relatively straightforward.
- _The input_ is a particulate matter sensor.
- _The output_ is Adafruit Neopixel addressable LEDs
- And **ESP32 microcontroller** _processes the logic_ outlined below to map air quality into colors. 
- A 3.7V, 2000 mAh Lithium-Polymer rechargeable battery powers the device.

![](Images/Electronics.jpg)
### Code


	include Adafruit_NeoPixel.h library
	define pin numbers (21 for LEDs, 5 PWM signal to sensor & A0 analog signal from sensor)
	declare NeoPixel strip object

	function setup{
		initialize NeoPixel strip
		declare the sensor PWM pin as output
	}

	function loop{
		define output_voltage & particle_density variables
		turn on LED pin to sensor
		delay 280 microseconds
		read sensor signal from analog pin
		delay 1 millisecond
		turn OFF LED pin to sensor
		compute output_voltage from analog signal (divide ADC value by 4095 since 12 bit)
		compute particle_density from output_voltage using linear equation from data sheet

		if (dust_density<50) {
			turn LEDs green
		}
		else if (dust_density>100) {
			turn LEDs red
		}
		else {
			turn LEDs orange
		}
		delay 2 seconds
	}

# Final presentation
Trought Arduino, a dust sensor, a strip of LED lights, digital design and digital fabrication children found and composed new meanings out of tales and provided a valuable contribution to the creative process. They learn about a lot of tools, materials, and ways of expressing themselves.
Our solution enhances the creative skills of society as a whole and promotes an open-minded and ‘out-of-the-box’ approach to life.
> **Alexandra and Helena sleep with Pascal and have a lot of fun hunting the smokers in the park**

![](Images/Testing_Pascal_park.png)

![](Images/Testing_Pascal_green.png) ![](Images/Testing_Pascal_red.png)

![Pascal and his leaf](Images/Pascal_and_leaf_final.jpg)
![](Images/Pascal_and_leaf_2.jpg)
![](Images/Leaf_bottom.jpg)
![](Images/Pascal_code_color.jpg)

Watch the video of the story of Pascal and his leaf :) ![Click here](Images/Pascals_story.mp4) 
## Future Development opportunities
Alexandra and Helena had several ideas for additional animals to help them monitor and study the environmental conditions around them.
### Tortoise to measure soil health
Soil health is at once incredibly important and difficult to measure, at it relies on a myriad of factors, from soil microbiome and earthworms to PH and carbon/nitrogen balance. We would like to design a tortoise that could, similarly to Pascal, measure one (or more) aspects of soil health.

![](Images/Turtle.jpg)
### Octopus to measure water pollution
Likewise, water pollution is a multifaceted issue. Some indicators that can be measured are the concentration of dissolved organic matter, conductivity, salinity, total dissolved solids, dissolved oxygen levels, and PH. A silicone molded octopus could be designed to measure this and foster water literacy.

![](Images/octopus.jpg)
### Dog to measure noise levels
Noise pollution affects millions of individuals living in urban environments. Transport (highways, trainlines and airports) is the number one source of noise pollution. A dog could be designed to measure the noise levels and demand actions from local authorities.
# Concepts
- [x] - Difference between principles of subtractive and additive manufacturing
- [x] - Identify and understand Types of 3D printing (e.g. FDM, SLA)
- [x] - 3D slicing
- [x] - Design for manufacture and design rules
- [x] - Printing resolution & associated printing time
- [x] - Understanding of different printing materials
- [x] - Types of 3D scanning (3.g. pointcloud, triangulation)
- [x] - 3D SENSING
- [x] - Parallel vs Series circuits
- [x] - Understanding role of basic electronics components (resistor, capacitor, diodes)
- [x] - Understanding the function of a microcontroller and associated pins
- [x] - Process and importance of debugging
- [x] - Design for manufacture CNC (tolerances)
- [x] - Analog/digital read/write
- [x] - Libraries (for arduino)
- [x] - Analog vs Digital inputs and outputs
- [x] - Understanding what is serial communication
- [x] - Understanding what memory is
- [x] - Toolchain,Compiling,Makefiles
- [x] - Debugging
- [x] - Algorithms & flow
# Skills
- [x] - Preparing CAD files for the 3D printer
- [x] - Command of slicing software (e.g. Cura)
- [x] - Command of 3D scanning software (e.g. Skanect)
- [ ] - Photogrammetry
- [ ] - Command of electronics software (e.g. Kicad)
- [ ] - Create PCB schematics in electronics software
- [ ] - Finalize files for PCB production
- [ ] - Soldering a milled board
- [x] - Knowing how to select correct components for a board - i.e. microcontroller
- [x] - Understanding parts and components on milling
- [x] - Command of 2D/3D software
- [ ] - Command of CAM software & G Code generation
- [ ] - Joint & Pocket design
- [x] - Retrieve relevant information from microcontroller datasheet
- [x] - Programming a board
- [x] - Installing libraries
- [ ] - Navigating & using platformio
- [ ] - Information theory
- [x] - Code structure organisation & planning
- [x] - Learn how to copy and remix code
- [ ] - Writing pseudo code
# Links to individual pages
Made with love by [Veronica](https://gitlab.com/ritaveronica.agreda.depazos/mdef-website), [Paco](https://paco_flores.gitlab.io/mdef-2021/) & [Clément](https://clement_rames.gitlab.io/mdef-website/)
